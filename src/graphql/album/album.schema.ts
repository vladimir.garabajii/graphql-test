import {
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
} from "graphql";
import {AlbumType} from "./album.type";

export const AlbumQuery = {
  Albums: {
    type: GraphQLList(AlbumType),
    resolve: async (_root: any, _args: any, ctx: any) => {
      return await ctx.db.all(`SELECT * FROM albums;`);
    }
  },
  Album: {
    type: GraphQLList(AlbumType),
    args: {
      ArtistId: {
        type: GraphQLNonNull(GraphQLID)
      }
    },
    resolve: async (_root: any, _args: any, ctx: any) => {
      return await ctx.db.all(`SELECT * FROM albums WHERE ArtistId = (?);`, [_args.ArtistId])
    }
  },
};
