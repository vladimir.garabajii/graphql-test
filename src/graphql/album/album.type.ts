import {GraphQLID, GraphQLObjectType, GraphQLString} from "graphql";

export const AlbumType = new GraphQLObjectType({
  name: "Album",
  fields: {
    AlbumId: { type: GraphQLID },
    Title: { type: GraphQLString },
    ArtistId: { type: GraphQLID }
  }
})
