import {GraphQLObjectType, GraphQLSchema} from "graphql";
import {AlbumQuery} from "./album/album.schema";
import {ArtistMutation, ArtistQuery, ArtistSubscription} from "./artist/artist.schema";

const queryType = new GraphQLObjectType({
  name: "Query",
  fields: {
    ...AlbumQuery,
    ...ArtistQuery
  }
})

const mutationType = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    ...ArtistMutation
  }
})

const subscriptionType = new GraphQLObjectType({
  name: "Subscription",
  fields: {
    ...ArtistSubscription
  }
})

export const schema = new GraphQLSchema({
  query: queryType,
  mutation: mutationType,
  subscription: subscriptionType
})
