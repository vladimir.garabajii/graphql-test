import {GraphQLID, GraphQLList, GraphQLObjectType, GraphQLString} from "graphql";
import {AlbumType} from "../album/album.type";

export const ArtistType = new GraphQLObjectType({
  name: "Artist",
  fields: {
    ArtistId: { type: GraphQLID },
    Name: { type: GraphQLString },
    Albums: {
      type: new GraphQLList(AlbumType),
      resolve: async (_root, _args, ctx) => {
        return await ctx.db.all(`SELECT * FROM albums WHERE ArtistId = (?);`, [_root.ArtistId])
      }
    }
  }
})
