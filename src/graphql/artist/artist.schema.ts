import {
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
  GraphQLString
} from "graphql";
import { PubSub } from 'graphql-subscriptions';
import {ArtistType} from "./artist.type";

const pubsub = new PubSub();

const ARTIST_UPDATED_TOPIC = 'artistUpdated'

export const ArtistQuery = {
  Artists: {
    type: GraphQLList(ArtistType),
    resolve: async (_root: any, _args: any, ctx: any) => {
      return await ctx.db.all(`SELECT * FROM artists ar;`);
    }
  },
  Artist: {
    type: ArtistType,
    args: {
      id: {
        type: GraphQLNonNull(GraphQLID)
      }
    },
    resolve: async (_root: any, _args: any, ctx: any) => {
      return await ctx.db.get(`SELECT * FROM artists WHERE ArtistId = (?);`, [_args.id])
    }
  },
};

export const ArtistMutation = {
  UpdateArtist: {
    type: ArtistType,
    args: {
      id: {
        type: GraphQLNonNull(GraphQLID)
      },
      name: {
        type: GraphQLNonNull(GraphQLString)
      },
    },
    resolve: async (_root: any, _args: any, ctx: any) => {
      await ctx.db.get(`UPDATE artists SET NAME = (?) WHERE ArtistId = (?);`, [_args.name, _args.id]);

      const updatedArtist = await ctx.db.get(`SELECT * FROM artists WHERE ArtistId = (?);`, [_args.id]);

      await pubsub.publish(ARTIST_UPDATED_TOPIC, {ArtistUpdated: updatedArtist})
      return updatedArtist;
    }
  }
}

export const ArtistSubscription = {
  ArtistUpdated: {
    type: ArtistType,
    subscribe: () => pubsub.asyncIterator(ARTIST_UPDATED_TOPIC)
  }
}
