import { server } from "./server";
import {schema} from "./graphql/schema";
import {execute, subscribe} from "graphql";
import { useServer } from 'graphql-ws/lib/use/ws';
import ws from 'ws';

const port = process.env.PORT || 8080;

const socket = server.listen(port, () => {
  console.log(`GraphQL server is running on port ${port}.`);

  const wsServer = new ws.Server({
    server: socket,
    path: '/subscriptions'
  });

  useServer(
    {
      schema,
      execute,
      subscribe
    },
    wsServer
  )
});
